Upscaler
========

Upscaler is a set of tools to help upscale image sequences using [EDSR](https://github.com/thstkdgus35/EDSR-PyTorch.git), a super resolution framework built around PyTorch.

This documentation is very much a work in progress.


# Prerequisites

To use the various tools included in this package, you need to install some dependencies
```bash
git submodule update --init --recursive 

sudo apt install imagemagick parallel python3-pip procmail
sudo apt install nvidia-cuda-dev nvidia-cuda-toolkit

sudo pip3 install torchvision scikit-image tqdm imageio
```


# Tools

The various tools are located inside the `scripts` subdirectory

- `divide`: divide all frames from an image sequence into chunks of size `1024 x 1024`
- `upscale`: upscale all frames from an image sequence to another directory
- `conquer`: merge frames from a divided and upscaled image sequence
- `select`: copy a selection of frames from a directory to another, based on their index
- `process`: uses all the previous tools to select, divide, upscale, and merge an image sequence


# Usage

Of all the tools described in the previous section, only `process` should be used as it handles the whole upscaling process by itself. It takes a jepg image sequence and upscales it into a png sequence. The reason that it takes jpegs as input is that the upscaling library, EDSR, sometimes have issues with transparency. Using jpegs eliminates any possibility for transparency.

Before running `process` you need to extract an image sequence from a video file. `ffmpeg` is recommended for this task, and a valid command line would be as follows:
```bash
ffmpeg -i INPUT_VIDEO_FILE -c:v mjpeg -q:v 2 /path/to/frames/frame_%05d.jpg
```

Once exported, you can run the following command to upscale the frames:
```bash
cd script
./process -i /path/to/frames -o /path/to/upscaled/frames
```
The output directory must already exist otherwise the command returns with an error.

Note that the frames are processed by batches of 100. If your GPU is powerful enough, it may be idling while the CPU is dividing the frames and merging their upscaled counterparts. If so, you can specify a batch range and run the script twice. For example say the image sequence has 50000 frames, which makes 500 batches. Running the script twice would look like this:
```bash
./process -i /path/to/frames -o /path/to/upscaled/frames -f 0 -l 250
./process -i /path/to/frames -o /path/to/upscaled/frames -f 251 -l 500
```

These options are also useful to run the upscaling process on multiple computers simultaneously.


# License
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version. See LICENSE for the license text.
